class SiteBuilderService
  def initialize(params={})
    @version_name = params[:version_name]
  end

  def self.call(*args)
    new(*args).build
  end

  def build
    build_site
  end

  private

  def build_site
    @configuration = SiteConfiguration.where('version_name = ?', @version_name).first
    @list_widgets = ListWidget.all.where('site_configuration_id = ?', @configuration.id).order(order: :asc)

    save_page
    update_configuration
  end

  def get_page
    ApplicationController.renderer.render(
      template: 'pages/build_site',
      assigns: {
        configuration: @configuration,
        list_widgets: @list_widgets
      }
    )
  end

  def save_page
    $Redis.set('build_site', get_page)
  end

  def update_configuration
    @configuration.version_status = 'Published'
    @configuration.publish_date = DateTime.now
    @configuration.save
  end
end