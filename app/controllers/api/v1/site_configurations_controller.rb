class Api::V1::SiteConfigurationsController < ApiApplicationController
  def create
    @configuration = SiteConfiguration.new(configuration_params)

    if @configuration.save
      builder_jobs
      render json: @configuration, status: :created
    else
      render json: @configuration.errors, status: :unprocessable_entity
    end
  end

  def version_status
    @configuration = SiteConfiguration.where('version_name = ?', params[:version_name]).select(:title, :version_name, :version_status, :publish_date).first
    render json: @configuration
  end

  private

  def configuration_params
    params.permit(
      :title,
      :icon_url,
      :banner_bg,
      :title_color,
      :site_bg,
      :list_widgets_attributes => [
          :title,
          :description,
          :widget_type,
          :video_url,
          :weather_info,
          :bg_color,
          :order
        ]
      )
  end

  def builder_jobs
    SiteBuildJob.perform_later(@configuration.version_name)
  end
end