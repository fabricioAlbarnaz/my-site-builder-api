class PagesController < ApplicationController
  # caches_page :build_site

  def index
    @page = $Redis.get('build_site').html_safe
  end

  def build_site
  end
end