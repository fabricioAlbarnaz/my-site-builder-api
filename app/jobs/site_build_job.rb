class SiteBuildJob < ApplicationJob
  queue_as :build_site

  def perform(version_name)
    sleep 10

    SiteBuilderService.call({ version_name: version_name })
  end
end
