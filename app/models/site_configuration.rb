class SiteConfiguration < ApplicationRecord
  before_create :generate_version
  has_many :list_widgets, dependent: :destroy

  validates_presence_of :title
  accepts_nested_attributes_for :list_widgets

  private

  def generate_version
    self.version_name = "version-#{DateTime.now.strftime('%Y%d%m%H%M%S')}"
  end
end
