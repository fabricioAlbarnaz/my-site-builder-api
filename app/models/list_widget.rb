class ListWidget < ApplicationRecord
  belongs_to :site_configuration

  validates_presence_of :order
  validates :order, numericality: true

  enum widget_type: {
    text: 1,
    weather: 2,
    video: 3
  }
end
