require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  namespace :api do
      namespace :v1 do
          resources :site_configurations, only: [:create]
      end
  end

  get 'version_status/:version_name', to: 'api/v1/site_configurations#version_status'

  get 'my_site', to: 'pages#index'
  get 'my_site_v2', to: 'pages#my_site_v2'
  get 'build_site', to: 'pages#build_site'
end

