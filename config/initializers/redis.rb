REDIS_URL = Rails.env === "test" ? ENV['REDIS_URL_TEST'] : ENV['REDIS_URL']

$Redis = Redis.new({ url: REDIS_URL })

$Redis.flushdb if Rails.env === "test"