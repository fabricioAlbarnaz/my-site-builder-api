# My Site Builder API

This project uses:

[Docker](https://www.docker.com/get-started)

[Docker Compose](https://docs.docker.com/compose/install)

## Setup this project
```sh
cp .env.example .env
```

Fill `.env` with the data needed

Build the containers (make sure your Docker is on)
```sh
docker-compose build
```

Create the database
```sh
docker-compose run web bundle exec rails db:create
docker-compose run web bundle exec rails db:migrate
```

And then build it up
```sh
docker-compose up --build
```

Wait it finish, and now we are online!

## Endpoints

To create your site configuration:

- POST `/api/v1/site_configurations`

An example of payload to this endpoint:
```json
{
  "title": "My new site",
  "icon_url": "http:/img-url",
  "banner_bg": "#ffffff",
  "title_color": "#000000",
  "site_bg": "#ffffff",
  "list_widgets_attributes": [
    {
      "order": 2,
      "title": "titulo 1",
      "description": "descricao",
      "widget_type": "text",
      "bg_color": "#ffffff",
      "video_url": ""
    },
    {
      "order": 1,
      "title": "titulo 2",
      "description": "descricao",
      "widget_type": "text",
    },
    {
      "order": 3,
      "widget_type": "video",
      "video_url": "https://www.youtube.com/watch?v=HotKY_kS0-Y"
    },
    {
      "order": 4,
      "widget_type": "weather",
      "weather_info": "Santa Maria - 12C"
    }
  ]
}
```

About the payload:

- `title`: `string` is the title of your site;
- `icon_url`: `string` the link to icon of your site;
- `banner_bg`: `string` is the background color of header banner (hexadecimal);
- `title_color`: `string` font color of the title (hexadecimal);
- `site_bg`: `string` color of body (hexadecimal);
- `list_widgets_attributes`: the list of widgets;
- `list_widgets_attributes`:
  - `order`: `integer` order that widget is loaded;
  - `type`: `string` values 'text', 'video', 'weader';
  - `title`: `string` title for text widget;
  - `bg_color`: `string` card background color for text widget (hexadecimal);
  - `description`: `string` description for text widget;
  - `video_url`: `string` url of a YouTube video for video widget;
  - `weather_info`: `string` info of weather for weather widget;

In the response of this request you will have `version_name`, that is the version of your configuration;

After you made the POST, it will automatically go to a queue to be processed;

To check your version status:

- GET `/version_status/YOUR-VERSION-NAME.json`

ex: `get /version_status/version-20203009000922.json`

You can build any existing version by running the maintenance task:
```sh
docker-compose run web rake my_site:build_site[yor-version-name]
```
ex: `docker-compose run web rake my_site:build_site[version-20203009000922]`

## Acess your site page

Just navegate to `/my_site`


## TESTS

To check the application tests, just run:
```sh
docker-compose run web bundle exec rspec
```

# Implementations

For this site builder api, was choose to use `redis` and `sidekiq` to run the build.

You can check the queue of builds accessing `/sidekiq` with user and password that you define in `.env`.

## The build

After a `post` that creates the site configuration, is called a job `SiteBuildJob` and queue the build request.

In this job was add a `sleep` to simulate a big process on build
```ruby
def perform(version_name)
  sleep 10

  SiteBuilderService.call({ version_name: version_name })
end
```

The build is created by `SiteBuilderService` that receives a version name.

To build the page, it renders a template
```ruby
def get_page
  ApplicationController.renderer.render(
    template: 'pages/build_site',
    assigns: {
      configuration: @configuration,
      list_widgets: @list_widgets
    }
  )
end
```

Then saves a cache in `redis`
```ruby
def save_page
  $Redis.set('build_site', get_page)
end
```

When you check your builded site, navigating to `/my_site`, it just render the cache in the `redis`
```ruby
def index
  @page = $Redis.get('build_site').html_safe
end
```