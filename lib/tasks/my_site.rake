namespace :my_site do
  desc "Building site..."
  task :build_site, [:version] => [:environment] do |t, args|
    puts "building version #{args[:version]}"

    SiteBuilderService.call({ version_name: args[:version] })

    puts "Done"
  end
end
