require 'rails_helper'

RSpec.describe 'pages/build_site' do
  let!(:conf) { SiteConfiguration.create!(
    title: 'My new site',
    icon_url: '',
    banner_bg: '#ff0000',
    title_color: '#000000',
    site_bg: '#ffffff',
    list_widgets_attributes: [
      {
        order: 0,
        title: 'Widget text',
        description: 'some description',
        widget_type: 'text',
        bg_color: '#ffffff'
      },
      {
        order: 1,
        widget_type: 'weather',
        weather_info: 'Santa Maria - 12c'
      },
      {
        order: 3,
        widget_type: 'video',
        video_url: 'http:some-url?v=123'
      },
    ]
  )}
  let(:widget) { ListWidget.all.where('site_configuration_id = ?', conf.id) }

  it 'render site configurations' do
    assign(:configuration, conf)
    assign(:list_widgets, widget)

    render
    expect(rendered).to match /My new site/
  end

  it 'render widget text' do
    assign(:configuration, conf)
    assign(:list_widgets, widget)

    render
    expect(rendered).to match /Widget text/
    expect(rendered).to match /My new site/
  end

  it 'render widget weather' do
    assign(:configuration, conf)
    assign(:list_widgets, widget)

    render
    expect(rendered).to match /Santa Maria - 12c/
  end
end