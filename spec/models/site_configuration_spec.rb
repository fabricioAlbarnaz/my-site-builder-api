require 'rails_helper'

RSpec.describe SiteConfiguration, type: :model do
  it { should have_many(:list_widgets).dependent(:destroy) }
  it { should validate_presence_of(:title) }

  it 'is valid with valid attributes' do
    expect(SiteConfiguration.new(title: 'some title')).to be_valid
  end

  it 'is invalid with invalid attributes' do
    expect(SiteConfiguration.new()).to_not be_valid
  end

  it 'creates a version name' do
    configuration = SiteConfiguration.new(title: 'My site')
    configuration.save
    expect(configuration.version_name).to match(/version-*/)
  end
end
