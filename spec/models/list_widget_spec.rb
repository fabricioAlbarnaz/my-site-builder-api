require 'rails_helper'

RSpec.describe ListWidget, type: :model do
  it { should belong_to(:site_configuration) }

  it { should validate_presence_of(:order) }
end
