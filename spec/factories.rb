FactoryBot.define do
  url_array = [
    "https://image.shutterstock.com/image-vector/phone-volume-vector-icon-on-260nw-1135845185.jpg",
    "https://woofbox.com.br/wp-content/uploads/2018/08/instagram-png-icon-5a3aafc62af570.341760101513795526176.jpg"
  ]
  video_array = ['video?v=13133123', 'video?v=2424234']
  type_array = ['text', 'video', 'weather']

  factory :random_configuration, class: SiteConfiguration do
    title { Faker::Lorem.word }
    icon_url { video_array.sample }
    banner_bg { Faker::Color.hex_color }
    title_color { Faker::Color.hex_color }
    site_bg { Faker::Color.hex_color }
  end


  factory :random_list_widget, class: ListWidget do
    title { Faker::Lorem.word }
    description { Faker::Lorem.word }
    widget_type { type_array.sample }
    video_url { video_array.sample }
    weather_info  { Faker::Lorem.word }
    bg_color { Faker::Color.hex_color }
    order { Faker::Number.number(digits: 1) }
  end
end

def site_configuration_with_widgets(widgets_counter: 2)
  FactoryBot.create(:random_configuration) do |configuration|
    FactoryBot.create_list(:random_list_widget, widgets_counter, site_configuration: configuration)
  end
end