require 'rails_helper'

RSpec.describe SiteBuilderService do
  let!(:configuration) { site_configuration_with_widgets(widgets_counter: 2) }
  let(:parameter) {{ version_name: configuration.version_name }}

  describe 'before service call' do
    it 'version is not builded' do
      expect(configuration.version_status).to eq('processing')
    end

    it 'has no publish date' do
      expect(configuration.publish_date).to be_nil
    end

    it 'site not stored in redis' do
      expect($Redis.get('build_site')).to be_nil
    end
  end

  describe 'after service call' do
    before do
      SiteBuilderService.call(parameter)
    end

    it 'version is builded' do
      expect(SiteConfiguration.find(configuration.id).version_status).to eq('Published')
    end

    it 'has publish date' do
      expect(SiteConfiguration.find(configuration.id).publish_date).to_not be_nil
    end

    it 'site stored in redis' do
      expect($Redis.get('build_site')).to_not be_nil
    end
  end
end