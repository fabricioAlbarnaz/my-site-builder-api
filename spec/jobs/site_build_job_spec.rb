require 'rails_helper'

RSpec.describe SiteBuildJob, type: :job do
  include ActiveJob::TestHelper

  let!(:configuration) { site_configuration_with_widgets(widgets_counter: 2) }

  subject(:job) { described_class.perform_later(configuration.version_name) }

  it 'queues the job' do
    expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'is in build_site queue' do
    expect(SiteBuildJob.new.queue_name).to eq('build_site')
  end

  it 'executes perform' do
    expect(SiteBuilderService).to receive(:call).with({ version_name: configuration.version_name })
    perform_enqueued_jobs { job }
  end

  after do
    clear_enqueued_jobs
    clear_performed_jobs
  end
end
