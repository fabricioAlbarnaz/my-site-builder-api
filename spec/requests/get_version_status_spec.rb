require 'rails_helper'

RSpec.describe 'get version status', :type => :request do

  let!(:configuration) { site_configuration_with_widgets(widgets_counter: 2) }

  before { get "/version_status/#{configuration.version_name}.json", as: :json }

  it 'returns status code 200' do
    expect(response).to have_http_status(:success)
  end
end