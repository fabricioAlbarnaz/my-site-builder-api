require 'rails_helper'

RSpec.describe 'post site_configuration route', :type => :request do
  describe 'with a valid reqest' do
    before do
      post '/api/v1/site_configurations', params: {
        title: 'My new site',
        icon_url: '',
        banner_bg: '#ff0000',
        title_color: '#000000',
        site_bg: '#ffffff',
        list_widgets_attributes: [
          {
            order: 0,
            title: 'Title 1',
            description: 'some description',
            widget_type: 'text',
            bg_color: '#ffffff'
          }
        ]
      }
    end

    it 'returns a created status' do
      expect(response).to have_http_status(:created)
    end

    it 'returns the site configuration' do
      expect(JSON.parse(response.body)['title']).to eq('My new site')
    end

    it 'returns the new site version' do
      expect(JSON.parse(response.body)['version_name']).to match(/version-*/)
    end

    it 'returns the info of processing version build' do
      expect(JSON.parse(response.body)['version_status']).to eq('processing')
    end

    it 'queues the job to build version' do
      expect(ActiveJob::Base.queue_adapter.enqueued_jobs.length).to eq(1)
    end
  end

  describe 'with an invalid request' do
    it 'returns unprocessable entity' do
      post '/api/v1/site_configurations', params: { description: 'some description' }
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end