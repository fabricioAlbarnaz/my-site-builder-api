class CreateListWidgets < ActiveRecord::Migration[6.0]
  def change
    create_table :list_widgets do |t|
      t.integer :widget_type
      t.string :title
      t.string :description
      t.string :bg_color
      t.string :video_url
      t.string :weather_info
      t.integer :order
      t.references :site_configuration, null: false, foreign_key: true

      t.timestamps
    end
  end
end
