class CreateSiteConfigurations < ActiveRecord::Migration[6.0]
  def change
    create_table :site_configurations do |t|
      t.string :title
      t.string :icon_url
      t.string :banner_bg
      t.string :title_color
      t.string :site_bg
      t.string :version_name
      t.string :version_status, default: 'processing'
      t.timestamp :publish_date

      t.timestamps
    end
  end
end
